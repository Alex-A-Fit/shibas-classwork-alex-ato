package com.example.shibaapi.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibaapi.R
import com.example.shibaapi.databinding.FragmentShibaBinding
import com.example.shibaapi.util.Resource
import com.example.shibaapi.util.ShibaFragAdapter
import com.example.shibaapi.viewmodel.ShibaViewModel


class ShibaFragment : Fragment() {

    private var _binding: FragmentShibaBinding? = null
    private val binding: FragmentShibaBinding get() = _binding!!

    private val viewModel by viewModels<ShibaViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentShibaBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding){
        shibaRv.layoutManager = GridLayoutManager(context, 2)
        viewModel.viewState.observe(viewLifecycleOwner){ viewState ->
            when(viewState) {
                is Resource.Error -> {}
                Resource.Loading -> {}
                is Resource.Success -> {
                    shibaRv.adapter = ShibaFragAdapter().apply {
                        applyData(viewState.data)
                    }
                }
            }

        }
    }
}