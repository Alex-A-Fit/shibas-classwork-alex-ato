package com.example.shibaapi.util

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shibaapi.databinding.ShibaImgBinding

class ShibaFragAdapter : RecyclerView.Adapter<ShibaFragAdapter.ViewHolder>() {

    private lateinit var data: List<String>

    class ViewHolder(private val binding:ShibaImgBinding): RecyclerView.ViewHolder(binding.root){
        fun applyImg(item: String) {
            binding.shibaImgIv.loadImage(item)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ShibaImgBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.applyImg(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    fun applyData(data: List<String>){
        this.data = data
    }

}
    private fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }
