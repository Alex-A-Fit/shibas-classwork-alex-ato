package com.example.shibaapi.util

sealed class Resource(data: List<String>?, errorMsg: String?){
    data class Success(val data: List<String>): Resource(data, null)
    object Loading: Resource(null, null)
    data class Error(val errorMsg: String ): Resource(null, errorMsg)
}