package com.example.shibaapi.model

import com.example.shibaapi.model.api.ApiService
import com.example.shibaapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object ShibaRepo {
    private val apiService by lazy { ApiService.retroFitInstance }

    suspend fun getShibes() = withContext(Dispatchers.IO){
        return@withContext try {
            val response = apiService.getShibes()
            if (response.isSuccessful && response.body()!!.isNotEmpty()){
                Resource.Success(response.body()!!)
            }else{
                Resource.Error("Something went terribly wrong")
            }
        } catch (e: Exception){
            e.localizedMessage?.let { Resource.Error(it) }
        }
    }
}