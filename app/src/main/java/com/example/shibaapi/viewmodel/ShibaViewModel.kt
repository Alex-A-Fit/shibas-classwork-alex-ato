package com.example.shibaapi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibaapi.model.ShibaRepo
import com.example.shibaapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibaViewModel: ViewModel() {

    val repo by lazy {ShibaRepo}

    private val _viewState: MutableLiveData<Resource> = MutableLiveData<Resource>()
    val viewState: LiveData<Resource> get() = _viewState

    init{
        viewModelScope.launch(Dispatchers.Main) {
            _viewState.value = repo.getShibes()
        }
    }
}